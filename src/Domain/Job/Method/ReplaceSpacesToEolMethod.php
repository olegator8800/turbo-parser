<?php
declare(strict_types=1);

namespace App\Domain\Job\Method;

class ReplaceSpacesToEolMethod implements JobMethodInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAlias(): string
    {
        return 'replaceSpacesToEol';
    }

    /**
     * {@inheritdoc}
     */
    public function processText(string $text): string
    {
        return str_replace(' ', PHP_EOL, $text);
    }
}
