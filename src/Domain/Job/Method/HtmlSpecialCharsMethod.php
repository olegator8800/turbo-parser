<?php
declare(strict_types=1);

namespace App\Domain\Job\Method;

class HtmlSpecialCharsMethod implements JobMethodInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAlias(): string
    {
        return 'htmlspecialchars';
    }

    /**
     * {@inheritdoc}
     */
    public function processText(string $text): string
    {
        return htmlspecialchars($text);
    }
}
