<?php
declare(strict_types=1);

namespace App\Domain\Job;

use App\Domain\Job\Job;

interface JobRepositoryInterface
{
    /**
     * @return Job|null
     */
    public function findJobIsReadyToProcessing(): ?Job;

    /**
     * @param Job $job
     */
    public function save(Job $job): void;
}
