<?php
declare(strict_types=1);

namespace App\Domain\Job\Status;

use App\Domain\Job\Exception\Status\IllegalStatusTransitionException;

abstract class AbstractJobStatus implements JobStatusInterface
{
    /**
     * {@inheritdoc}
     */
    public function readyToProcessing()
    {
        throw new IllegalStatusTransitionException();
    }

    /**
     * {@inheritdoc}
     */
    public function processing()
    {
        throw new IllegalStatusTransitionException();
    }

    /**
     * {@inheritdoc}
     */
    public function done()
    {
        throw new IllegalStatusTransitionException();
    }
}
