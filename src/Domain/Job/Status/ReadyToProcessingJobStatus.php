<?php
declare(strict_types=1);

namespace App\Domain\Job\Status;

class ReadyToProcessingJobStatus extends AbstractJobStatus
{
    /**
     * {@inheritdoc}
     */
    public function processing()
    {
        return new ProcessingJobStatus();
    }
}
