<?php
declare(strict_types=1);

namespace App\Domain\Job;

use App\Domain\Job\Method\JobMethodCollection;
use App\Domain\Job\Status\JobStatusInterface;
use App\Domain\Job\Status\ReadyToProcessingJobStatus;
use App\Domain\Job\Status\ProcessingJobStatus;
use App\Domain\Job\Status\DoneJobStatus;
use App\Domain\Job\Exception\EmptyTextException;
use App\Domain\Job\Exception\EmptyMethodsException;

class Job
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var JobMethodCollection
     */
    private $methods;

    /**
     * @var JobStatusInterface
     */
    private $status;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable
     */
    private $updatedAt;

    /**
     * @param string $text
     * @param JobMethodCollection  $methods
     *
     * @throws EmptyTextException If $text param is empty
     * @throws EmptyMethodsException If $methods collection is empty
     */
    public function __construct(string $text, JobMethodCollection $methods)
    {
        if (!$text = trim($text)) {
            throw new EmptyTextException();
        }

        if ($methods->isEmpty()) {
            throw new EmptyMethodsException();
        }

        $this->text = $text;
        $this->methods = $methods;
        $this->status = new ReadyToProcessingJobStatus();

        $this->createdAt = new \DateTimeImmutable();
        $this->changeUpdateAt();
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return JobMethodCollection
     */
    public function getMethods(): JobMethodCollection
    {
        return $this->methods;
    }

    /**
     * @return boolean
     */
    public function isReadyToProcessing(): bool
    {
        return ($this->status instanceof ReadyToProcessingJobStatus);
    }

    public function readyToProcessing(): void
    {
        $this->setStatus($this->status->readyToProcessing());
    }

    /**
     * @return boolean
     */
    public function isProcessing(): bool
    {
        return ($this->status instanceof ProcessingJobStatus);
    }

    public function processing(): void
    {
        $this->setStatus($this->status->processing());
    }

    /**
     * @return boolean
     */
    public function isDone(): bool
    {
        return ($this->status instanceof DoneJobStatus);
    }

    public function done(): void
    {
        $this->setStatus($this->status->done());
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @param JobStatusInterface $status
     */
    private function setStatus(JobStatusInterface $status): void
    {
        $this->status = $status;
        $this->changeUpdateAt();
    }

    protected function changeUpdateAt(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
