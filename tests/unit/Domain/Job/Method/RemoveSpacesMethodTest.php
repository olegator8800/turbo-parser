<?php
declare(strict_types=1);

use App\Domain\Job\Method\RemoveSpacesMethod;

class RemoveSpacesMethodTest extends \Codeception\Test\Unit
{
    /**
     * @test
     */
    public function processText()
    {
        $method = new RemoveSpacesMethod();

        $text = 'test test  test   test';
        $expectedText = 'testtesttesttest';

        $this->assertEquals($method->processText($text), $expectedText);
    }

    /**
     * @test
     */
    public function getAlias()
    {
        $method = new RemoveSpacesMethod();

        $this->assertEquals($method->getAlias(), 'removeSpaces');
    }
}
