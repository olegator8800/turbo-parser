<?php
declare(strict_types=1);

namespace App\Domain\Job\Exception;

class EmptyTextException extends \DomainException implements JobExceptionInterface
{
    const MESSAGE = 'Text cannot be empty';

    /**
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = self::MESSAGE, int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
