<?php
declare(strict_types=1);

use Codeception\Util\Stub;
use Helper\Unit;

use App\Domain\Job\Job;
use App\Domain\Job\Method\JobMethodCollection;
use App\Domain\Job\Exception\EmptyTextException;
use App\Domain\Job\Exception\EmptyMethodsException;
use App\Domain\Job\Exception\Status\IllegalStatusTransitionException;

class JobTest extends \Codeception\Test\Unit
{
    /**
     * @test
     */
    public function createJobByEmptyTextException()
    {
        $this->expectException(EmptyTextException::class);
        $this->expectExceptionMessage(EmptyTextException::MESSAGE);

        new Job($emptyText = '', $collection = Unit::createJobMethodCollection());
    }

    /**
     * @test
     */
    public function createJobByEmptyMethodsException()
    {
        $this->expectException(EmptyMethodsException::class);
        $this->expectExceptionMessage(EmptyMethodsException::MESSAGE);

        new Job($text = 'text', $emptyCollecyion = new JobMethodCollection([]));
    }

    /**
     * @test
     */
    public function jobGetText()
    {
        $text = 'text text';

        $job = new Job($text, $collection = Unit::createJobMethodCollection());

        $this->assertEquals($job->getText(), $text);
    }

    /**
     * @test
     */
    public function newJobReadyToProcessingStatus()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());

        $this->assertTrue($job->isReadyToProcessing());
        $this->assertFalse($job->isProcessing());
        $this->assertFalse($job->isDone());
    }

    /**
     * @test
     */
    public function jobStatusFlow()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());

        $job->processing();

        $this->assertFalse($job->isReadyToProcessing());
        $this->assertTrue($job->isProcessing());
        $this->assertFalse($job->isDone());

        $job->done();

        $this->assertFalse($job->isReadyToProcessing());
        $this->assertFalse($job->isProcessing());
        $this->assertTrue($job->isDone());
    }

    /**
     * @test
     */
    public function jobStatusChangeReady_To_ProcessingAtDoneException()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());

        $this->expectException(IllegalStatusTransitionException::class);
        $job->done();
    }

    /**
     * @test
     */
    public function jobStatusChangeReady_To_ProcessingAtReady_To_ProcessingException()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());

        $this->expectException(IllegalStatusTransitionException::class);
        $job->readyToProcessing();
    }

    /**
     * @test
     */
    public function jobStatusChangeProcessingAtReady_To_ProcessingException()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());
        $job->processing();

        $this->expectException(IllegalStatusTransitionException::class);
        $job->readyToProcessing();
    }

    /**
     * @test
     */
    public function jobStatusChangeProcessingAtProcessingException()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());
        $job->processing();

        $this->expectException(IllegalStatusTransitionException::class);
        $job->processing();
    }

    /**
     * @test
     */
    public function jobStatusChangeDoneAtReady_To_ProcessingException()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());
        $job->processing();
        $job->done();

        $this->expectException(IllegalStatusTransitionException::class);
        $job->readyToProcessing();
    }

    /**
     * @test
     */
    public function jobStatusChangeDoneAtDoneException()
    {
        $job = new Job($text = 'text', $collection = Unit::createJobMethodCollection());
        $job->processing();
        $job->done();

        $this->expectException(IllegalStatusTransitionException::class);
        $job->done();
    }
}
