<?php
namespace Helper;
// here you can define custom actions
// all public methods declared in helper class will be available in $I

use App\Domain\Job\Method\JobMethodCollection;
use App\Domain\Job\Method\RemoveSpacesMethod;
use Codeception\Util\Stub;

class Unit extends \Codeception\Module
{
    public static function createJobMethodCollection()
    {
        $methood = Stub::make(RemoveSpacesMethod::class);

        return new JobMethodCollection([$methood]);
    }
}
