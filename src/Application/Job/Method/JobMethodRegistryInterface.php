<?php
declare(strict_types=1);

namespace App\Application\Job\Method;

use App\Domain\Job\Method\JobMethodInterface;

interface JobMethodRegistryInterface
{
    /**
     * @param  string $alias
     *
     * @return JobMethodInterface
     */
    public function get(string $alias): JobMethodInterface;
}
