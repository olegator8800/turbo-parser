<?php
declare(strict_types=1);

namespace App\Domain\Job\Status;

use App\Domain\Job\Exception\Status\IllegalStatusTransitionException;

interface JobStatusInterface
{
    /**
     * @return JobStatusInterface
     *
     * @throws IllegalStatusTransitionException
     */
    public function readyToProcessing();

    /**
     * @return JobStatusInterface
     *
     * @throws IllegalStatusTransitionException
     */
    public function processing();

    /**
     * @return JobStatusInterface
     *
     * @throws IllegalStatusTransitionException
     */
    public function done();
}
