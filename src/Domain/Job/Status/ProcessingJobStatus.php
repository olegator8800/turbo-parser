<?php
declare(strict_types=1);

namespace App\Domain\Job\Status;

class ProcessingJobStatus extends AbstractJobStatus
{
    /**
     * {@inheritdoc}
     */
    public function done()
    {
        return new DoneJobStatus();
    }
}
