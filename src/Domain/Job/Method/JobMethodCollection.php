<?php
declare(strict_types=1);

namespace App\Domain\Job\Method;

use App\Domain\Job\Exception\Method\UnexpectedTypeCollectionException;

class JobMethodCollection implements \Countable, \Iterator
{
    /**
     * @var array
     */
    private $methodList;

    /**
     * @param array $methodList
     */
    public function __construct(array $methodList)
    {
        $this->validateList($methodList);

        $this->methodList = $methodList;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->methodList);
    }

    public function rewind()
    {
        reset($this->methodList);
    }

    /**
     * @return JobMethodInterface|null
     */
    public function current()
    {
        return current($this->methodList);
    }

    /**
     * @return string
     */
    public function key()
    {
        return key($this->methodList);
    }

    /**
     * @return JobMethodInterface|null
     */
    public function next()
    {
        return next($this->methodList);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        $key = key($this->methodList);

        return ($key !== null && $key !== false);
    }

    /**
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return !count($this->methodList);
    }

    /**
     * @param  array  $methodList
     *
     * @throws UnexpectedTypeCollectionException
     */
    private function validateList(array $methodList)
    {
        foreach ($methodList as $method) {
            if (!is_object($method) || !($method instanceof JobMethodInterface)) {
                throw new UnexpectedTypeCollectionException($method);
            }
        }
    }
}
