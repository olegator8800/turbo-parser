<?php
declare(strict_types=1);

use App\Domain\Job\Method\HtmlSpecialCharsMethod;

class HtmlSpecialCharsMethodTest extends \Codeception\Test\Unit
{
    /**
     * @test
     */
    public function processText()
    {
        $method = new HtmlSpecialCharsMethod();

        $text = "<a href='test'>Test</a>";
        $expectedText = htmlspecialchars($text);

        $this->assertEquals($method->processText($text), $expectedText);
    }

    /**
     * @test
     */
    public function getAlias()
    {
        $method = new HtmlSpecialCharsMethod();

        $this->assertEquals($method->getAlias(), 'htmlspecialchars');
    }
}
