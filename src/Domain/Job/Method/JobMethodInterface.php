<?php
declare(strict_types=1);

namespace App\Domain\Job\Method;

interface JobMethodInterface
{
    /**
     * @return string
     */
    public function getAlias(): string;

    /**
     * @param  string $text
     *
     * @return text
     */
    public function processText(string $text): string;
}
