<?php
declare(strict_types=1);

use Codeception\Util\Stub;

use App\Domain\Job\Job;
use App\Domain\Job\Method\JobMethodCollection;
use App\Domain\Job\Method\RemoveSpacesMethod;
use App\Domain\Job\Method\StripTagsMethod;
use App\Application\Job\JobBroker;
use App\Application\Job\Persistence\MemoryJobRepository;
use App\Application\Job\JobResultDTO;

class JobBrokerTest extends \Codeception\Test\Unit
{
    private function createJob($text, $methods)
    {
        $methodCollection = new JobMethodCollection($methods);

        return new Job($text, $methodCollection);
    }

    private function createJobRepositoryReturnJob(Job $job)
    {
        return Stub::make(
            MemoryJobRepository::class,
            [
                'findJobIsReadyToProcessing' => function() use($job) {
                    $job->processing();

                    return $job;
                }
            ]
        );
    }

    private function createJobRepositoryNotReturnJob()
    {
        return Stub::make(
            MemoryJobRepository::class,
            [
                'findJobIsReadyToProcessing' => function() {
                    return null;
                }
            ]
        );
    }

    /**
     * @test
     */
    public function processOneJobSingleMethod()
    {
        $text = 'test text test   text';
        $methods = [
            new RemoveSpacesMethod(),
        ];

        $expectedText = 'testtexttesttext';

        $job = $this->createJob($text, $methods);
        $jobRepository = $this->createJobRepositoryReturnJob($job);

        $jobBroker = new JobBroker($jobRepository);

        $result = $jobBroker->processOneJob();

        $this->assertTrue($result instanceof JobResultDTO);
        $this->assertEquals($result->text, $expectedText);
    }

    /**
     * @test
     */
    public function processOneJobManyMethods()
    {
        $text = '<p>Параграф.</p><!-- Комментарий --> <a href="#fragment">Еще текст</a>';
        $methods = [
            new StripTagsMethod(),
            new RemoveSpacesMethod(),
        ];

        $expectedText = 'Параграф.Ещетекст';

        $job = $this->createJob($text, $methods);
        $jobRepository = $this->createJobRepositoryReturnJob($job);

        $jobBroker = new JobBroker($jobRepository);

        $result = $jobBroker->processOneJob();

        $this->assertTrue($result instanceof JobResultDTO);
        $this->assertEquals($result->text, $expectedText);
    }

    /**
     * @test
     */
    public function processOneJobWhenNotJobFind()
    {
        $jobRepository = $this->createJobRepositoryNotReturnJob();

        $jobBroker = new JobBroker($jobRepository);

        $result = $jobBroker->processOneJob();

        $this->assertTrue($result === null);
    }
}
