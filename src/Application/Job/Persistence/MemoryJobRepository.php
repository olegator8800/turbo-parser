<?php
declare(strict_types=1);

namespace App\Application\Job\Persistence;

use App\Domain\Job\Job;
use App\Domain\Job\Method\JobMethodCollection;
use App\Domain\Job\JobRepositoryInterface;
use App\Application\Job\Method\JobMethodRegistryInterface;

class MemoryJobRepository implements JobRepositoryInterface
{
    /**
     * @var JobMethodRegistryInterface
     */
    private $methodRegistry;

    /**
     * @param JobMethodRegistryInterface $methodRegistry
     */
    public function __construct(JobMethodRegistryInterface $methodRegistry)
    {
        $this->methodRegistry = $methodRegistry;
    }

    /**
     * {@inheritdoc}
     */
    public function findJobIsReadyToProcessing(): ?Job
    {
        //mocking data
        $methodsAlias = [
            'stripTags',
            'replaceSpacesToEol',
        ];

        $methods = [];
        /**
         * Здесь можно например сделать try catch(MethodNotFoundException)
         * И применив паттерн стратегия как то по разному обрабатывать ситуацию
         * когда алиас не найден, например для устаревших версий
         */
        foreach ($methodsAlias as $alias) {
            $methods[] = $this->methodRegistry->get($alias);
        }

        $text = '<p>Параграф.</p><!-- Комментарий --> <a href="#fragment">Еще текст</a>';

        $methodCollection = new JobMethodCollection($methods);

        $job = new Job($text, $methodCollection);

        $job->processing();
        $this->save($job);

        return $job;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Job $job): void
    {
        $methods = $job->getMethods();

        $methodsAlias = [];

        foreach ($methods as $method) {
            $methodsAlias[] = $method->getAlias();
        }

        //$methodsAlias save or serialize, etc.. from storage
    }
}
