<?php
declare(strict_types=1);

namespace App\Application\Job;

use App\Domain\Job\JobRepositoryInterface;

class JobBroker
{
    /**
     * @var JobRepositoryInterface
     */
    private $jobRepository;

    /**
     * @param JobRepositoryInterface $jobRepository
     */
    public function __construct(JobRepositoryInterface $jobRepository)
    {
        $this->jobRepository = $jobRepository;
    }

    /**
     * @return App\Domain\Job\JobResultDTO|null
     */
    public function processOneJob(): ?JobResultDTO
    {
        $job = $this->jobRepository->findJobIsReadyToProcessing();

        if (!$job) {
            return null;
        }

        $text = $job->getText();

        foreach ($job->getMethods() as $method) {
            $text = $method->processText($text);
        }

        $job->done();
        $this->jobRepository->save($job);

        $jobResult = new JobResultDTO();
        $jobResult->text = $text;

        return $jobResult;
    }
}
