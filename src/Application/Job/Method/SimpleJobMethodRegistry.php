<?php
declare(strict_types=1);

namespace App\Application\Job\Method;

use App\Domain\Job\Method\JobMethodInterface;
use App\Domain\Job\Method\StripTagsMethod;
use App\Domain\Job\Method\RemoveSpacesMethod;
use App\Domain\Job\Method\ReplaceSpacesToEolMethod;
use App\Domain\Job\Method\HtmlSpecialCharsMethod;
use App\Application\Job\Exception\Method\MethodNotFoundException;

class SimpleJobMethodRegistry implements JobMethodRegistryInterface
{
    /**
     * @var array
     */
    private $registry = [];

    public function __construct()
    {
        //Любая реализация, закидывать сервисы по тегам через di контейнер, фабрика...
        //mocking
        $this->add(new StripTagsMethod());
        $this->add(new RemoveSpacesMethod());
        $this->add(new ReplaceSpacesToEolMethod());
        $this->add(new HtmlSpecialCharsMethod());
    }

    /**
     * @param  JobMethodInterface $method
     */
    private function add(JobMethodInterface $method)
    {
        $this->registry[$method->getAlias()] = $method;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $alias): JobMethodInterface
    {
        if (!array_key_exists($alias, $this->registry)) {
            throw new MethodNotFoundException($alias);
        }

        return $this->registry[$alias];
    }
}
