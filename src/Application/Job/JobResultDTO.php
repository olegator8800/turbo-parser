<?php
declare(strict_types=1);

namespace App\Application\Job;

class JobResultDTO
{
    /**
     * @var string
     */
    public $text;
}
