<?php
declare(strict_types=1);

namespace App\Domain\Job\Exception\Method;

use App\Domain\Job\Exception\JobExceptionInterface;

class UnexpectedTypeCollectionException extends \UnexpectedValueException implements JobExceptionInterface
{
    const MESSAGE_TEMPLATE = 'Unexpected method type from collection, given "%s"';

    /**
     * @param mixed           $value
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($value, int $code = 0, \Throwable $previous = null)
    {
        $message = sprintf(self::MESSAGE_TEMPLATE, is_object($value) ? get_class($value) : gettype($value));

        parent::__construct($message, $code, $previous);
    }
}
