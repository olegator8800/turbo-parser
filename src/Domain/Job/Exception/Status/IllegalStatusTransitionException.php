<?php
declare(strict_types=1);

namespace App\Domain\Job\Exception\Status;

use App\Domain\Job\Exception\JobExceptionInterface;

class IllegalStatusTransitionException extends \DomainException implements JobExceptionInterface
{
    const MESSAGE = 'Illegal status transition';

    /**
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = self::MESSAGE, int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
