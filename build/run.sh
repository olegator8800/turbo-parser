#!/usr/bin/env bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

if ! docker ps -q &> /dev/null
then
    echo "You must be in docker group or root"
    exit 1
fi

docker network create turbo-parser

cd ${DIR}/

docker-compose --project-name turbo-parser up --build -d turbo-parser-nginx
docker-compose --project-name turbo-parser run --rm turbo-parser-php-fpm /usr/bin/composer install --no-plugins --no-scripts
docker-compose --project-name turbo-parser run --rm turbo-parser-php-fpm chown -R `id -u $USER`:`id -u $GROUP` .
