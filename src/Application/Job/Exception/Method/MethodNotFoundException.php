<?php
declare(strict_types=1);

namespace App\Application\Job\Exception\Method;

use App\Application\Job\Exception\JobExceptionInterface;

class MethodNotFoundException extends \RuntimeException implements JobExceptionInterface
{
    const MESSAGE_TEMPLATE = 'Method "%s" not found by alias from registry';

    /**
     * @param string          $key
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct($key, int $code = 0, \Throwable $previous = null)
    {
        $message = sprintf(self::MESSAGE_TEMPLATE, $key);

        parent::__construct($message, $code, $previous);
    }
}
