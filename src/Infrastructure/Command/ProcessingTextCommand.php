<?php
declare(strict_types=1);

namespace App\Infrastructure\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Application\Job\JobBroker;

class ProcessingTextCommand extends Command
{
    /**
     * @var JobBroker
     */
    private $jobBroker;

    public function __construct(JobBroker $jobBroker)
    {
        $this->jobBroker = $jobBroker;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:processing-text')
            ->setDescription('Perform jobs on text processing')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        while ($result = $this->jobBroker->processOneJob()) {
            var_dump($result); //в задаче дальше информации нет что делать с результатом :)
            die; //тк в примере всегда одна и та же задача
        }
    }
}
