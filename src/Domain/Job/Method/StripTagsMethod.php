<?php
declare(strict_types=1);

namespace App\Domain\Job\Method;

class StripTagsMethod implements JobMethodInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAlias(): string
    {
        return 'stripTags';
    }

    /**
     * {@inheritdoc}
     */
    public function processText(string $text): string
    {
        return strip_tags($text);
    }
}
