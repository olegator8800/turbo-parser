<?php
declare(strict_types=1);

namespace App\Domain\Job\Method;

class RemoveSpacesMethod implements JobMethodInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAlias(): string
    {
        return 'removeSpaces';
    }

    /**
     * {@inheritdoc}
     */
    public function processText(string $text): string
    {
        return str_replace(' ', '', $text);
    }
}
