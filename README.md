turbo-parser
======

#### Как развернуть
* Запуск docker окружения:

```bash
./build/run.sh
```
* Создать `.env` по аналогии с `.env.dist`

* Команда для запуска обработки текста
```bash
docker exec -ti turbo-parser-php-fpm bin/console app:processing-text
```

#### Тесты
* Запуск тестов:
```bash
docker exec -ti turbo-parser-php-fpm php vendor/bin/codecept run
```
